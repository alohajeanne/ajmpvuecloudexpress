const Promise = require('es6-promise').Promise

class HttpRequest {
  constructor() {
    this.ip = 'https://mh5.bl.com/h5_gateway'
    this.defaultConfig = {
      method: 'POST',
      header: {
        'content-type': 'application/json',
        'chnflg': 'h5'
      }
    }
    this.getRequest = this.httpsPromisify(wx.request)
  }
  httpsPromisify(fn) {
    return function (obj = {}) {
      return new Promise((resolve, reject) => {
        obj.success = function (res) {
          resolve(res.data)
        }
        obj.fail = function (res) {
          reject(res)
        }
        fn(obj)
      })
    }
  }
  $get(url, data, dataType, header = {}) {
    return this.getRequest({
      url: this.ip + url,
      data,
      method: 'GET',
      dataType,
      header: Object.assign({}, this.defaultConfig.header, header)
    })
  }
  $post(url, data, dataType, header = {}) {
    return this.getRequest({
      url: this.ip + url,
      data,
      method: 'POST',
      dataType,
      header: Object.assign({}, this.defaultConfig.header, header)
    })
  }
  $ajax(obj = {}) {
    return this.getRequest(Object.assign({}, this.defaultConfig, obj))
  }
}

export const api = new HttpRequest()
