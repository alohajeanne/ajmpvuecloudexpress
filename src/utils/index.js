import { api } from '@/utils/api'
import Vue from 'vue'

function formatNumber (n) {
  const str = n.toString()
  return str[1] ? str : `0${str}`
}

export function formatTime (date) {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  const t1 = [year, month, day].map(formatNumber).join('/')
  const t2 = [hour, minute, second].map(formatNumber).join(':')

  return `${t1} ${t2}`
}

// 提示弹框
const Toast = (msg) => {
  wx.showToast({
    title: msg,
    icon: 'none',
    duration: 2000
  })
}

// 加载按钮
const Loading = () => {
  wx.showLoading({
    title: '请稍等哦~',
    mask: true
  })
}

// 关闭加载按钮
const CloseLoading = () => {
  setTimeout(function () {
    wx.hideLoading()
  }, 800)
}

// 微信授权
function wxLogin (e) {
  if (e.mp.detail.userInfo) {
    wx.checkSession({
      success () {
        console.log('checkSession success')
        thirdPartLogin(e, wx.getStorageSync('wxCode'))
      },
      fail () {
        console.log('checkSession fail')
        // session_key 已经失效，需要重新执行登录流程
        wx.login({
          complete (res) {
            console.log(res)
            thirdPartLogin(e, res.code)
          }
        })
      }
    })
  }
}

// 绑定百联用户信息
function thirdPartLogin(e, code) {
  if (code) {
    // 获取用户信息的授权情况
    wx.setStorageSync('wxInfo', e.mp.detail)
    let params = {
      appType: Vue.$APP_ID,
      code: code,
      encryptedData: e.mp.detail.encryptedData,
      iv: e.mp.detail.iv
    };
    // 获取解密信息接口
    api.$post('/commonwx/getUserInfoByCode.htm', params).then(data => {
      let resData = JSON.parse(data.obj)
      if (resData.data) {
        // let openId = resData.data.openId;
        // 要传unionId
        let unionId = resData.data.unionId;
        wx.setStorageSync("unionId", unionId);
        // 第三方ID绑定登录
        api.$post('/baseinfo/jn/queryByThirdPartyUid.htm', {
          third_party_id: unionId,
          third_party_id_type: "1"
        }).then(res => {
          if (res.msg) {
            // Toast(res.msg)
            // 没有获取到用户信息 跳转登录页
            wx.redirectTo({
              url: '/pages/login/main'
            })
          } else {
            let json = JSON.parse(res.obj);
            wx.setStorageSync("member_id", json.member_id);
            wx.setStorageSync("member_token", json.member_token);
            wx.setStorageSync("blUserInfo", json);
            wx.navigateBack({
              delta: 1
            });
            Toast('已授权');
          }
        })
      } else {
        if (resData.errmsg) {
          Toast(resData.errmsg)
        }
      }
    })
  } else {
    Toast('授权失败')
  }
}

// 查看是否登录
function isLogin() {
  if (!wx.getStorageSync('blUserInfo')) {
    wx.navigateTo({
      url: '/pages/authorize/main'
    })
  }
}

export default {
  formatNumber,
  formatTime,
  Toast,
  wxLogin,
  isLogin,
  Loading,
  CloseLoading
}
