import { api } from '@/utils/api'

export default {
  // 获取解密信息接口
  getDecryptInfoByCode: params => {
    return api.$post('/commonwx/getUserInfoByCode.htm', params)
  },
  // 获取短信验证码
  getMassageCode: params => {
    return api.$post('/baseinfo/jn/sendRegisterOrLoginValidateCodeMessage.htm', params)
  },
  // 第三方ID绑定登录
  queryByThirdPartyUid: params => {
    return api.$post('/baseinfo/jn/queryByThirdPartyUid.htm', params)
  },
  // 注册登录并绑定
  weixinRegisterLoginAndBind: params => {
    return api.$post('/baseinfo/jn/registerAndLoginWithThirdPartyUidByCode.htm', params)
  }
}
