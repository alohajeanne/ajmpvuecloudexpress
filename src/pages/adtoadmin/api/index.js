import { api } from '@/utils/api'

export default {
    receiveAddress: params => {
        return api.$post('/receiveAddress/queryAddressList.htm', params)
    },
    deleteAddress: params => {
        return api.$post('/receiveAddress/deleteAddress.htm', params)
    }
}
