import { api } from '@/utils/api'

export default {
	// 获取详情
  queryCourier: params => {
    return api.$post('/orderLogistics/orderLogisticsByExpressNo.htm', params)
  }
}
