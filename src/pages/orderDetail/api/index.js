import { api } from '@/utils/api'

export default {
  orderLogisticsByExpressNo: params => {
    return api.$post('/orderLogistics/orderLogisticsByExpressNo.htm', params)
  },
  // 添加评价
  addComment: params => {
    return api.$post('/comment/addComment.htm', params)
  },
	// 取消订单
	orderCancelByOrderNo: params => {
		return api.$post('/order/orderCancelByOrderNo.htm', params)
	}
}
