import { api } from '@/utils/api'

export default {
  // 查询保价比例
  getInsuranceScale: params => {
    return api.$post('/insuranceValue/getInsuranceScale.htm', params)
  }
}
