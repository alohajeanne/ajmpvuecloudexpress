import { api } from '@/utils/api'

export default {
	// 查询当日寄件
  queryFindCompany: params => {
    return api.$post('/queryExpress/queryToDaySendExpress.htm', params)
  },
  queryCourier: params => {
    return api.$post('/orderLogistics/orderLogisticsByExpressNo.htm', params)
  }
}
