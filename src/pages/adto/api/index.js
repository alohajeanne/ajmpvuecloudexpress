import { api } from '@/utils/api'

export default {
    queryAddress: params => {
        return api.$post('/receiveAddress/queryAddress.htm', params)
    },
    updateReceiveAddress: params => {
        return api.$post('/receiveAddress/updateReceiveAddress.htm', params)
    },
    addReceiveAddress: params => {
        return api.$post('/receiveAddress/addReceiveAddress.htm', params)
    }
}
