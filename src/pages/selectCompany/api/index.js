import { api } from '@/utils/api'

export default {
	// 查询快递公司
  queryFindCompany: params => {
    return api.$post('/carriers/querySend.htm', params)
  }
}
