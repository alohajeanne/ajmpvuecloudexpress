import { api } from '@/utils/api'

export default {
	// 取消订单
	orderCancelByOrderNo: params => {
    	return api.$post('/order/orderCancelByOrderNo.htm', params)
  	},
	// 寄件数据
  	querySendExpress: params => {
    	return api.$post('/queryExpress/querySendExpress.htm', params)
	},
	// 评价
	addComment: params => {
    	return api.$post('/comment/addComment.htm', params)
	}
}
