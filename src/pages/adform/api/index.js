import { api } from '@/utils/api'

export default {
    addMailingAddress: params => {
        return api.$post('/mailingAddress/addMailingAddress.htm', params)
    },
    queryAddress: params => {
        return api.$post('/mailingAddress/queryAddress.htm', params)
    },
    updateMailingAddress: params => {
        return api.$post('/mailingAddress/updateMailingAddress.htm', params)
    }
}
