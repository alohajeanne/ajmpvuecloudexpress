import { api } from '@/utils/api'

export default {
	// 查询取件时间
    queryCollectTime: params => {
        return api.$get('/collectTime/queryCollectTime.htm', params)
    }
}
