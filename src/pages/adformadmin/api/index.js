import { api } from '@/utils/api'

export default {
    mailingAddress: params => {
        return api.$post('/mailingAddress/queryAddressList.htm', params)
    },
    setDefault: params => {
        return api.$post('/mailingAddress/setDefault.htm', params)
    },
    deleteAddress: params => {
        return api.$post('/mailingAddress/deleteAddress.htm', params)
    }
}
