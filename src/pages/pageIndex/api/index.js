import { api } from '@/utils/api'

export default {
  getChallengeList: params => {
    return api.$post('/weChatSports/getChallengeList.htm', params)
  },
  getChallenge: params => {
    return api.$post('/weChatSports/getChallenge.htm', params)
  },
  saveQuestionAnswer: params => {
    return api.$post('/weChatSports/saveQuestionAnswer.htm', params)
  }
}
