import { api } from '@/utils/api'

export default {
  // 查询寄件地址列表
  querySendAddress: params => {
    return api.$post('/mailingAddress/queryAddressList.htm', params)
  },
  // 查询收件地址列表
  queryReceiveAddress: params => {
    return api.$post('/receiveAddress/queryAddressList.htm', params)
  },
  // 查询收件地址列表
  getDefaultMonthAccount: params => {
    return api.$post('/settlementAccount/getDefaultMonthAccount.htm', params)
  },
  // 下单
  submitOrder: params => {
    return api.$post('/order/submitOrder.htm', params)
  },
  // 查询物品类型
  getProductType: params => {
    return api.$post('/item/selectItems.htm', params)
  },
  // 查询预估到达时间
  queryAnticipateTime: params => {
    return api.$post('/collectTime/queryAnticipateTime.htm', params)
  },
  // 查询预估运费(不含保价金额)
  selectOrderFreightAmount: params => {
    return api.$post('/order/selectOrderFreightAmount.htm', params)
  }
}
