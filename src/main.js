import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false
App.mpType = 'app'
var isIPX = false

// 百联云递
Vue.$APP_ID = Vue.prototype.$APP_ID = 'wx6f897475d893f1dc' // 输入小程序appid
Vue.$assetsPath = Vue.prototype.$assetsPath = '/static/assets'
// Vue.$wxInfo = Vue.prototype.$wxInfo = wx.getStorageSync('wxInfo') || ''

// Jeanne iPhone X 样式兼容
wx.getSystemInfo({
  complete: function (res) {
  	console.log(res.model)
    if (String(res.model).indexOf('iPhone X') != -1) {
    	isIPX = true
      Vue.$isIPX = Vue.prototype.$isIPX = isIPX
    } else if (String(res.model).indexOf('iPhone11') != -1) {
    	isIPX = true
    	Vue.$isIPX = Vue.prototype.$isIPX = isIPX
    }
  }
})

const app = new Vue(App)
app.$mount()
